import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Navigation extends Component {
    render() {
        return (
            <section id="navigation">
                <ul>
                    <li><Link to='/water/video'><p><strong>Видео</strong></p></Link></li>
                    <li><Link to='/water/game'><p><strong>Игры</strong></p></Link></li>
                </ul>
            </section>
        )
    }
}
