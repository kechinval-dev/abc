import React, { Component } from 'react';

import Navigation from './Navigation.jsx'

export default class WaterMenu extends Component {
  render() {
    return (
        <div className="row cn-nav-block">
          <div className="col-sm-6 align-self-center">
            <img src="/assets/water.png" alt="water" />
          </div>
          <div className="col-sm-6 align-self-center">
            <Navigation />
          </div>
        </div>
    )
  }
}
