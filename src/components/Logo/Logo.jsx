import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Logo extends Component{
    render() {
        return(
            <Link to='/'>
                <section id="logotype">
                    <div className="row justify-content-center">
                        <div className="d-inline"><img src="/assets/alphabet/акрас.png" alt="А" /></div>
                        <div className="d-inline"><img src="/assets/alphabet/ззел.png" alt="З" /></div>
                        <div className="d-inline"><img src="/assets/alphabet/боранж.png" alt="Б" /></div>
                        <div className="d-inline"><img src="/assets/alphabet/усирен.png" alt="У" /></div>
                        <div className="d-inline"><img src="/assets/alphabet/кфиол.png" alt="К" /></div>
                        <div className="d-inline"><img src="/assets/alphabet/аоранж.png" alt="А" /></div>
                    </div>
                    <div className="row justify-content-center">
                        <div className="d-inline"><img src="/assets/alphabet/бкрасн.png" alt="Б" /></div>
                        <div className="d-inline"><img src="/assets/alphabet/егол.png" alt="Е" /></div>
                        <div className="d-inline"><img src="/assets/alphabet/роранж.png" alt="Р" /></div>
                        <div className="d-inline"><img src="/assets/alphabet/есирен.png" alt="Е" /></div>
                        <div className="d-inline"><img src="/assets/alphabet/жроз.png" alt="Ж" /></div>
                        <div className="d-inline"><img src="/assets/alphabet/лсирен.png" alt="Л" /></div>
                        <div className="d-inline"><img src="/assets/alphabet/изел.png" alt="И" /></div>
                        <div className="d-inline"><img src="/assets/alphabet/вкрасн.png" alt="В" /></div>
                        <div className="d-inline"><img src="/assets/alphabet/озел.png" alt="О" /></div>
                        <div className="d-inline"><img src="/assets/alphabet/сгол.png" alt="С" /></div>
                        <div className="d-inline"><img src="/assets/alphabet/троз.png" alt="Т" /></div>
                        <div className="d-inline"><img src="/assets/alphabet/исирен.png" alt="И" /></div>
                    </div>
                </section>
            </Link>
        )
    }
}