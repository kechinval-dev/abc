import React, { Component } from 'react';
import "../../../../node_modules/video-react/dist/video-react.css";
import { Player } from 'video-react';

export default class EnergyVideo extends Component {
  render() {
    return (
      <div className="row align-items-center video">
        <div className="col-sm-6">
          <Player playsInline src="/assets/video/fixiki_1.mp4" />
        </div>
      </div>
    )
  }
}
