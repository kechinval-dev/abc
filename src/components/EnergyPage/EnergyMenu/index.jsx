import React, { Component } from 'react';

import Navigation from './Navigation.jsx'

export default class EnergyMenu extends Component {
  render() {
    return (
        <div className="row cn-nav-block">
          <div className="col-sm-6 align-self-center">
            <img src="/assets/lampa.png" alt="lampochka" />
          </div>
          <div className="col-sm-6 align-self-center">
            <Navigation />
          </div>
        </div>
    )
  }
}
