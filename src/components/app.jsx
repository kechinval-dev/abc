import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Logo from './Logo/Logo.jsx';

import HomePage from './HomePage/index.jsx';

import EnergyMenu from './EnergyPage/EnergyMenu/index.jsx';
import EnergyVideo from './EnergyPage/EnergyVideo/index.jsx';
import EnergyGame from './EnergyPage/EnergyGame/index.jsx';

import WaterMenu from './WaterPage/WaterMenu/index.jsx';
import WaterVideo from './WaterPage/WaterVideo/index.jsx';
import WaterGame from './WaterPage/WaterGame/index.jsx';

import WasteMenu from './WastePage/WasteMenu/index.jsx';
import WasteVideo from './WastePage/WasteVideo/index.jsx';
import WasteGame from './WastePage/WasteGame/index.jsx';

import NoPage from './404/404.jsx';

class Routes extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/energy" component={EnergyMenu} />
        <Route path='/energy/video' component={EnergyVideo} />
        <Route path='/energy/game' component={EnergyGame}/>
        <Route exact path="/water" component={WaterMenu} />
        <Route path="/water/video" component={WaterVideo} />
        <Route path='/water/game' component={WaterGame} />
        <Route exact path='/waste' component={WasteMenu} />
        <Route path='/waste/video' component={WasteVideo}/>
        <Route path='/waste/game' component={WasteGame}/>
        <Route component={NoPage} />
      </Switch>
    )
  }
}

function App() {
  return (
    <Router>
      <div className="App container">
        <div className="row justify-content-center">
          <Logo />
        </div>
        <Route component={Routes} />
      </div>
    </Router>
  );
}

export default App;