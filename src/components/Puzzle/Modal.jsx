import React, { Component } from 'react';
import Modal from 'react-bootstrap4-modal';

export default class ModalWindow extends Component {

    constructor(props, context) {
        super(props, context);
    
        this.handleShow = this.handleShow.bind(this);
        this.handleAgain = this.handleAgain.bind(this);
        this.handleNext = this.handleNext.bind(this);
    
        this.state = {
            show: this.props.show
        };
    };
    
    handleShow() {
        this.setState({ show: true });
    };
    
    handleAgain() {
        this.props.action();
        this.setState({ show: false });
    };

    handleNext(){
        window.location.reload()
        this.setState({show: false});
    };

    render() {

        var {title, text} = this.props

        return (
            <div>
                <Modal dialogClassName='modal-dialog-centered' visible={this.state.show} onClickBackdrop={this.modalBackdropClicked}>
                    <div className="modal-header">
                        <h5 className="modal-title">{title}</h5>
                    </div>
                    <div className="modal-body">
                        <p>{text}</p>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" onClick={() => this.handleAgain()}>
                            Заново
                        </button>
                        <button type="button" className="btn btn-primary" onClick={() => this.handleNext()}>
                            Следующее слово
                        </button>
                    </div>
                </Modal>
            </div>
        );
    }
}