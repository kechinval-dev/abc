import React, { Component } from 'react';
import { DragSource } from 'react-dnd';

const itemSoure = {
  beginDrag(props) {
    return props.letter;
  },
  endDrag( props, monitor ) {
    if(!monitor.didDrop()){
      return;
    }
    return props.handleDrop(props.letter.id);
  }
}

function collect(connect, monitor){
  return{
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging()
  }
}

class Letter extends Component {
  render() {

    const { isDragging, connectDragSource, letter, id } = this.props;
    const opacity = isDragging ? 0 : 1;

    return connectDragSource(
      <div id={id} className='btn btn-light ml-1 mt-3 letter' style={{opacity}}>
        <span>{letter.letter}</span>
      </div>
    )
  }
}

export default DragSource('letter', itemSoure, collect)(Letter);