import React, { Component } from 'react';
import MultiBackend from 'react-dnd-multi-backend';
import HTML5toTouch from 'react-dnd-multi-backend/lib/HTML5toTouch';
import { DragDropContext } from 'react-dnd';

// import './puzzle.css'

import Dictionary from './Dictionary/Dictionary.json'

import Letter from './Letter.jsx';
import Target from './Target.jsx';
import ModalWindow from './Modal.jsx';
// import Navigation from '../../Header/Navigation/index.jsx'
// import Salute from './Salute.jsx';

class Puzzle extends Component {

    state = {
        letters: [],
        word: [],
        width: null,
        checkWord: null,
    };

    getWord(){
        let words = Object.keys(Dictionary);
        let shuffledArray = this.shuffleArray(words);
        this.setState({checkWord: shuffledArray[0]})
        return this.getLettersFromWord(shuffledArray[0]);
    };

    getLettersFromWord = (array) => {
        let letters = array.split('');
        let lettersArray = [...this.state.letters];

        letters.map((letter, id) => {
            return(
                lettersArray.push({
                id: id,
                letter: letter.toUpperCase()
                })
            )
        });
        
        let shuffledArray = this.shuffleArray(lettersArray);

        this.setState({letters: shuffledArray})

        this.setInputWidth(lettersArray)
    };

    setInputWidth (array) {
        let inputLenght = 50 * array.length;
        this.setState({width: inputLenght});
    }

    hideLetter = (id, letter) => {
        let word = [...this.state.word];
        word.push(letter.letter);
        this.setState({word: word.join('')});
        document.getElementById(id).style.visibility = 'hidden';
    };

    shuffleArray = array => {
        for (let i = array.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1));
            let temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        return array;
    };

    callMmodal(){
        let word = this.state.word;
        word = word.toString().toLowerCase();

        if (this.state.word.length === this.state.letters.length && this.state.checkWord === word) {
            return <ModalWindow 
                        show={true} 
                        title='Отлично' 
                        text='Так держать! Хочешь ещё? Тогда нажми на кнопку "Следующее слово"!' 
                        action={this.getWordAgain.bind(this)} />
        } else if (this.state.word.length === this.state.letters.length && this.state.checkWord !== word){
            return <ModalWindow 
                        show={true} 
                        title='Плохо' 
                        text='Не грусти :( Если хочешь попробовать ещё раз, нажми кнопку "Заново"!'
                        action={this.getWordAgain.bind(this)} />
        }
    };

    // callSalute(){
    //     let word = this.state.word;
    //     word = word.toString().toLowerCase();

    //     if (this.state.word.length === this.state.letters.length && this.state.checkWord === word) {
    //         return <Salute />
    //     }
    // }

    getWordAgain() {
        this.setState({
            word: []
        })
        let letters = document.getElementById('letters').querySelectorAll('.letter');
        for (let i = 0; i < letters.length; i++){
            letters[i].style.visibility = 'visible';
        }
    }

    componentWillMount(){
        this.getWord();
    }

    componentDidMount() {
        this.initialState = this.state;
    }

    render() {

        return(
            <div>
                {/* {this.callSalute()} */}
                {/* <Navigation /> */}
                <div className="bgmain bgmain_day" style={{top: '0px'}}>
                    <div className="bgmain2"></div>
                </div>
                <div className='vertical-center background'>
                    <div className='container-fluid'>
                        <div className='mx-auto row h-100'>
                            <div className='col my-auto'>
                                <Target word={this.state.word} width={this.state.width}/>
                            </div>
                        </div>
                        <div className='mx-auto row h-100'>
                            <div id='letters' className='col align-self-center'>
                                {this.state.letters.map(letter => {
                                    return(
                                        <Letter id={letter.id} key={letter.id} letter={letter} onClick="" handleDrop={(id) => this.hideLetter(id, letter)}/>
                                    )
                                })}
                            </div>
                        </div>
                        {this.callMmodal()}
                    </div>
                </div>
            </div>
        )
    }
}

export default DragDropContext(MultiBackend(HTML5toTouch))(Puzzle)
