import React, { Component } from 'react';
import { DropTarget } from 'react-dnd';

function collect(connect, monitor) {
    return{
        connectDropTarget: connect.dropTarget(),
        hovered: monitor.isOver(),
        letter: monitor.getItem(),
    }
}

class Target extends Component {
    render() {

        const { connectDropTarget, hovered, word, width } = this.props;
        const border = hovered ? 'dashed 1px gray' : "solid 1px white";

        return connectDropTarget(
            <div className='mx-auto puzzle' style={{
                border: border, 
                width: width, 
                height: '53px', 
                borderRadius: '10px'}}>
                <span className='word'>{word}</span>
            </div>
        )
    }
}

export default DropTarget('letter', {}, collect)(Target);