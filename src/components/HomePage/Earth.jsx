import React, { Component } from 'react';

export default class Earth extends Component {
  render() {
    return (
      <section id="earth">
        <img src="/assets/eye.png" alt="eye" className="eye_l" />
        <img src="/assets/eye.png" alt="eye" className="eye_r" />
        <img src="/assets/smile.png" alt="smile" className="smile" />
        <img src="/assets/earth.png" alt="earth" className="earth"/>
      </section>
    )
  }
}
