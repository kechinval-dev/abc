import React, { Component } from 'react';
import { Link } from "react-router-dom";

export default class Navigation extends Component {
    render() {
        return (
            <section id="navigation">
                <ul>
                    <li><Link to='/energy'><p><strong>Энергосбережение</strong></p></Link></li>
                    <li><Link to='/water'><p><strong>Экономия воды</strong></p></Link></li>
                   <li> <Link to='/waste'><p><strong>Раздельный сбор отходов</strong></p></Link></li>
                </ul>
            </section>
        )
    }
}

