import React, { Component } from 'react';

import Earth from './Earth.jsx';
import Navigation from './Navigation.jsx';

export default class HomePage extends Component {
  render() {
    return (
      <div className="row cn-nav-block">
        <div className="col-sm-6">
          <Earth />
        </div>
        <div className="col-sm-6 align-self-center">
          <Navigation />
        </div>
      </div>
    );
  }
}
